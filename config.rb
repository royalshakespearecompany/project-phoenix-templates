require "susy"
require "breakpoint"
require "sass-media_query_combiner"
css_dir = '/css'
sass_dir = '/sass'
javascripts_dir = '/js'
output_style = :expanded
relative_assets = true
