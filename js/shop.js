$(function () {

    /*
    ======== CODE BELOW IS FOR GREEDY NAV IF NEEDED OTHERWISE REMOVE ========
     */
    
    var $nav = $('.greedy-nav');
    var $btn = $('.greedy-nav button');
    var $vlinks = $('.greedy-nav .visible-links');
    var $hlinks = $('.greedy-nav .hidden-links');

    var breaks = [];

    function updateNav() {

        var availableSpace = $btn.hasClass('hidden') ? $nav.width() : $nav.width() - $btn.width() - 30;

        // The visible list is overflowing the nav
        if ($vlinks.width() > availableSpace) {

            // Record the width of the list
            breaks.push($vlinks.width());

            // Move item to the hidden list
            $vlinks.children().last().prependTo($hlinks);

            // Show the dropdown btn
            if ($btn.hasClass('hidden')) {
                $btn.removeClass('hidden');
            }

            // The visible list is not overflowing
        } else {

            // There is space for another item in the nav
            if (availableSpace > breaks[breaks.length - 1]) {

                // Move the item to the visible list
                $hlinks.children().first().appendTo($vlinks);
                breaks.pop();
            }

            // Hide the dropdown btn if hidden list is empty
            if (breaks.length < 1) {
                $btn.addClass('hidden');
                $hlinks.addClass('hidden');
            }
        }

        // Keep counter updated
       // $btn.attr("count", breaks.length);

        // Recur if the items are still overflowing the nav
        if ($vlinks.width() > availableSpace) {
            updateNav();
        }
    }

    // Window listeners

    $(window).resize(function () {
        updateNav();
    });

    $btn.on('click', function () {
        $hlinks.toggleClass('hidden');
    });

    updateNav();

    // product offer details show/hide
    $('#viewDialogBox').click(function(e) {
        e.preventDefault();
        if($(this).closest('.offer-panel').find('.offer').is(':visible'))
        {
            $(this).closest('.offer-panel').find('.offer').slideUp();
            $(this).html('Find out more &gt;&gt;');
        }
        else
        {
            $(this).closest('.offer-panel').find('.offer').slideDown();
            $(this).html('&lt;&lt; Hide details');
        }
    });

    // product gallery
    $('.popup-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile mfp-no-margins',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
        },
        image: {
            verticalFit: true,
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
        }
    });
});