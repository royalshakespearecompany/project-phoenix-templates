function gallerySetup(theGallery, theThumbs, thePhotos) {
    var $galleryThumbs = $("#" + theThumbs), $galleryPhotos = $("#" + thePhotos), $galleryThumbsContainer = $("#" + theThumbs + "cont"), $galleryPhotosContainer = $("#" + thePhotos + "cont"), $this = $(theGallery), i = 0;
    $galleryThumbs.children().each(function() {
        $(this).attr("data-thumb", i), i++;
    });
    var slideCount = i;
    $galleryThumbs.find("figure img").each(function() {
        $(this).addClass($(this).width() > $(this).height() ? "max-width" : "max-height");
    }), $galleryThumbs.slick({
        infinite: !0,
        arrows: !0,
        dots: !1,
        touchThreshold: 50,
        speed: 750,
        variableWidth: !0,
        asNavFor: "#" + thePhotos,
        focusOnSelect: !0,
        mobileFirst: !0
    }), $galleryThumbsContainer.prepend('<span class="slick-prev-custom" id="slickPrevCustom-' + theThumbs + '" role="button">Previous</span>').append('<span class="slick-next-custom" id="slickNextCustom-' + theThumbs + '" role="button">Next</span>'), 
    $("#slickPrevCustom-" + theThumbs).click(function() {
        $galleryThumbs.slick("slickGoTo", $("#" + theThumbs + " .slick-current").prev().attr("data-slick-index"));
    }), $("#slickNextCustom-" + theThumbs).click(function() {
        $galleryThumbs.slick("slickGoTo", $("#" + theThumbs + " .slick-current").next().attr("data-slick-index"));
    }), $galleryThumbs.on("click", $galleryThumbs.children(".gallery-thumb"), function() {
        $("body, html").scrollTop(Math.floor($this.offset().top - $(".header-container .header-top").height()));
    }), $galleryPhotos.on("init", function(event, slick) {
        $galleryThumbs.find('.gallery-thumb[data-thumb="0"]').addClass("active-thumb"), 
        $galleryPhotosContainer.find(".slide-number").text("1/" + slideCount);
    }), $galleryPhotos.find("figure img").each(function() {
        $(this).addClass($(this).width() > $(this).height() ? "max-width" : "max-height");
    }), $galleryPhotos.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: !1,
        arrows: !0,
        fade: !0,
        touchThreshold: 5,
        adaptiveHeight: !0,
        asNavFor: "#" + theThumbs
    }).on("beforeChange", function(event, slick, currentSlide, nextSlide) {
        $galleryThumbs.find(".active-thumb").removeClass("active-thumb"), $galleryThumbs.find('.gallery-thumb[data-thumb="' + nextSlide + '"]').addClass("active-thumb"), 
        $galleryPhotosContainer.find(".slide-number").text(nextSlide + 1 + "/" + slideCount);
    });
}

function navBtns(theButtons, theSubs) {
    theButtons.each(function(i) {
        var theThis = $(this), theListEntry = theThis.parent("li"), theBkgrnd = subNavBkgrnd;
        $(this).click(function(e) {
            e.preventDefault(), $(theSubs[i]).hasClass("vis") ? (theSubs.removeClass("vis-fade"), 
            theButtons.removeClass("vis-fade"), $(theSubs[i]).removeClass("vis").addClass("vis-fade"), 
            theThis.removeClass("vis").addClass("vis-fade"), theBkgrnd.removeClass("vis").addClass("vis-fade"), 
            theListEntry.removeClass("vis").addClass("vis-fade"), setTimeout(function() {
                theThis.removeClass("vis-fade"), $(theSubs[i]).removeClass("vis-fade"), theBkgrnd.removeClass("vis-fade"), 
                theListEntry.removeClass("vis-fade"), window.innerWidth >= largeScreen && theBody.removeClass("noscroll");
            }, 150)) : (theMenuButton.hasClass("mb-active") || (theMenuButton.addClass("mb-active"), 
            theBurger.addClass("active")), theButtons.removeClass("vis"), theThis.toggleClass("vis"), 
            theSubs.removeClass("vis"), $(theSubs[i]).toggleClass("vis"), mainNavLis.removeClass("vis"), 
            theListEntry.addClass("vis"), theBkgrnd.hasClass("vis") || theBkgrnd.addClass("vis"), 
            window.innerWidth >= largeScreen && !theBody.hasClass("noscroll") && theBody.addClass("noscroll"));
        });
    });
}

function globalSearchShow() {
    globalSearch.addClass("vis"), window.innerWidth >= largeScreen && theBody.addClass("noscroll");
}

function globalSearchHide() {
    globalSearch.addClass("vis-fade").removeClass("vis"), setTimeout(function() {
        globalSearch.removeClass("vis-fade"), window.innerWidth >= largeScreen && theBody.removeClass("noscroll");
    }, 300);
}

function edResourceLink() {
    var playLink, typeLink, ageLink, newLink = "/education/teacher-resources/search/";
    playLink = "" != resBankFilters.play ? resBankFilters.play : "any", ageLink = resBankFilters.age.length > 0 ? resBankFilters.age.join("-") : "any", 
    typeLink = resBankFilters.type.length > 0 ? resBankFilters.type.join("-") : "any", 
    newLink += "play/" + playLink + "/type/" + typeLink + "/age/" + ageLink + "/", $("#resButton").removeClass("inactive").attr("href", newLink);
}

function expandInnerDetails(event) {
    event.stopPropagation();
    var myMod = event.data.theMod;
    myMod.hasClass("selected") || ("undefined" == typeof expandedModule || "undefined" == expandedModule ? (expandedModule = myMod, 
    myMod.hasClass("day") && openDayAnimation()) : collapseInnerDetails(event));
}

function collapseInnerDetails(e) {
    if (e.stopPropagation(), "undefined" == typeof expandedModule || "undefined" == expandedModule) $("#openPerfs").length > 0 && closeDayAnimation(); else {
        var myMod = e.data.theMod;
        myMod.hasClass("day") && closeDayAnimation(), expandedModule = myMod, setTimeout(function() {
            dsOffset = expandedModule.offset().top, TweenLite.to(theBody, .1, {
                scrollTop: dsOffset - 90
            }), myMod.hasClass("day") && openDayAnimation();
        }, 450);
    }
}

function hsScrollMe(event) {
    var distance, thePosition = event.data.thePosition, theFC = event.data.theFC, theDirection = event.data.theDirection, theWidth = $(theFC).outerWidth(!0);
    distance = "left" == theDirection ? "-=" + theWidth.toString() : "+=" + theWidth.toString(), 
    $(horzScollers[thePosition]).find(".simplebar-scroll-content").animate({
        scrollLeft: distance
    }, 400);
}

function showIt(event) {
    var myTarget = $(event.target), myTargetID = myTarget.prop("id"), myTargetInner = myTarget.html(), expandID = "#" + myTargetID.replace("Expand", "") + "Hidden", myExpand = $(expandID);
    myExpand.show(), myTarget.animate({
        opacity: 0
    }, 100), TweenLite.from(myExpand, .4, {
        height: 0,
        opacity: 0,
        ease: Circ.easeOut,
        onComplete: function() {
            myTarget.unbind("click").removeClass("more-button").addClass("less-button").html("close").animate({
                opacity: 1
            }, 100).click(function(e) {
                e.preventDefault(), closeIt(myTargetID, myTargetInner, expandID);
            });
        }
    });
}

function closeIt(target, copy, expand) {
    var myExpand = $(expand), myTarget = $("#" + target);
    myTarget.animate({
        opacity: 0
    }, 50), TweenLite.to(myExpand, .4, {
        height: 0,
        opacity: 0,
        ease: Circ.easeOut,
        onComplete: function() {
            myExpand.css({
                height: "",
                opacity: "",
                display: ""
            }), myTarget.unbind("click").removeClass("less-button").addClass("more-button").html(copy).animate({
                opacity: 1
            }, 50).click(function(e) {
                e.preventDefault(), showIt(e);
            });
        }
    });
}

function getQueryVariable(variable) {
    if ("" == variable) return !1;
    for (var query = window.location.search.substring(1), vars = query.split("&"), i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == variable) return pair[1];
    }
    return !1;
}

function multipleQueries() {
    var query = window.location.search.substring(1), vars = query.split("&");
    return vars.length > 1 ? !0 : !1;
}

function videoResize(largeVid, smlVid) {
    var videoHoldWidth, videoHoldHeight;
    "undefined" != typeof largeVid && largeVid.length > 0 && largeVid.each(function() {
        videoHoldWidth = $(this).width(), videoHoldHeight = Math.round(videoHoldWidth / 16 * 9), 
        $(this).find("iframe").height(videoHoldHeight);
    }), "undefined" != typeof smlVid && smlVid.length > 0 && smlVid.each(function() {
        videoHoldWidth = $(this).width(), videoHoldHeight = Math.round(videoHoldWidth / 16 * 9), 
        $(this).find("iframe").height(videoHoldHeight);
    }), "undefined" != typeof galleryIframe && galleryIframe.length > 0 && galleryIframe.each(function() {
        videoHoldWidth = $(this).width(), videoHoldHeight = Math.round(videoHoldWidth / 16 * 9), 
        $(this).height(videoHoldHeight);
    }), "undefined" != typeof homeVidIframe && homeVidIframe.length > 0 && (videoHoldHeight = homeVideoCover.height(), 
    videoHoldWidth = Math.round(videoHoldHeight / 9 * 16), videoHoldWidth > window.innerWidth ? (videoHoldWidth = window.innerWidth, 
    videoHoldHeight = Math.round(window.innerWidth / 16 * 9)) : homeVideoHolder.find("#videoCover").is(":visible") || (videoHoldHeight = Math.min(Math.round(window.innerWidth / 1440 * 684), 684), 
    videoHoldWidth = Math.round(videoHoldHeight / 9 * 16)), homeVidIframe.height(videoHoldHeight));
}

function callPlayer(frame_id, func, args) {
    function messageEvent(add, listener) {
        var w3 = add ? window.addEventListener : window.removeEventListener;
        w3 ? w3("message", listener, !1) : (add ? window.attachEvent : window.detachEvent)("onmessage", listener), 
        console.log(w3);
    }
    console.log("callPlayer called, frame_id: " + frame_id + ", func: " + func + ", args: " + args), 
    window.jQuery && frame_id instanceof jQuery && (frame_id = frame_id.get(0).id);
    var iframe = document.getElementById(frame_id);
    iframe && "IFRAME" != iframe.tagName.toUpperCase() && (iframe = iframe.getElementsByTagName("iframe")[0]), 
    callPlayer.queue || (callPlayer.queue = {});
    var queue = callPlayer.queue[frame_id], domReady = "complete" == document.readyState;
    if (domReady && !iframe) window.console && console.log("callPlayer: Frame not found; id=" + frame_id), 
    queue && clearInterval(queue.poller); else if ("listening" === func) console.log("listening buzz"), 
    iframe && iframe.contentWindow && (func = '{"event":"listening","id":' + JSON.stringify("" + frame_id) + "}", 
    iframe.contentWindow.postMessage(func, "*")); else if (domReady && (!iframe || iframe.contentWindow && (!queue || queue.ready)) && (queue && queue.ready || "function" != typeof func)) {
        if (iframe && iframe.contentWindow) {
            if (func.call) return func();
            iframe.contentWindow.postMessage(JSON.stringify({
                event: "command",
                func: func,
                args: args || [],
                id: frame_id
            }), "*");
        }
    } else console.log("I'm going to set off that listening buzz"), queue || (queue = callPlayer.queue[frame_id] = []), 
    queue.push([ func, args ]), "poller" in queue || (queue.poller = setInterval(function() {
        callPlayer(frame_id, "listening");
    }, 250), messageEvent(1, function runOnceReady(e) {
        if (console.log("e= " + e.data), !iframe) {
            if (console.log("no iframe"), iframe = document.getElementById(frame_id), !iframe) return;
            if ("IFRAME" != iframe.tagName.toUpperCase() && (iframe = iframe.getElementsByTagName("iframe")[0], 
            !iframe)) return;
        }
        if (e.source === iframe.contentWindow) for (clearInterval(queue.poller), queue.ready = !0, 
        messageEvent(0, runOnceReady); tmp = queue.shift(); ) callPlayer(frame_id, tmp[0], tmp[1]);
    }, !1));
}

function getFrameID(id) {
    var elem = document.getElementById(id);
    if (elem) {
        if (/^iframe$/i.test(elem.tagName)) return id;
        var elems = elem.getElementsByTagName("iframe");
        if (!elems.length) return null;
        for (var i = 0; i < elems.length && !/^https?:\/\/(?:www\.)?youtube(?:-nocookie)?\.com(\/|$)/i.test(elems[i].src); i++) ;
        if (elem = elems[i], elem.id) return elem.id;
        do id += "-frame"; while (document.getElementById(id));
        return elem.id = id, id;
    }
    return null;
}

function onYouTubePlayerAPIReady() {
    YT_ready(!0);
}

function isTouchDevice() {
    return 1 == ("ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch || navigator.maxTouchPoints > 0 || navigator.msMaxTouchPoints > 0);
}

function arrayAdd(a, b) {
    return a + b;
}

function closeInfo(pNumb) {
    var thePopup = $(infoPopUp[pNumb]), myPos = parseInt(thePopup.css("bottom"));
    thePopup.removeClass("here"), TweenLite.to(thePopup, .4, {
        bottom: -30,
        opacity: 0,
        ease: Circ.easeIn,
        onComplete: function() {
            thePopup.css("display", "none");
        }
    });
    for (var pNumbIt = pNumb + 1; pNumbIt < infoPopUp.length; ) {
        if ($(infoPopUp[pNumbIt]).hasClass("here")) {
            setTimeout(function() {
                moveInfoPop(pNumbIt, myPos);
            }, 200);
            break;
        }
        pNumbIt++;
    }
}

function moveInfoPop(pNumb, newPos) {
    var myPos = $(infoPopUp[pNumb]).height() + 8 + newPos;
    TweenLite.to($(infoPopUp[pNumb]), .4, {
        bottom: newPos,
        ease: Circ.easeInOut,
        onComplete: function() {
            pNumb < infoPopUp.length - 1 && moveInfoPop(pNumb + 1, myPos);
        }
    });
}

function perfFilter(myFilters) {
    var theCheckbox = myFilters.checkBox || null, theStart = myFilters.startDate || plActiveFilters.when[0], theEnd = myFilters.endDate || plActiveFilters.when[1];
    plWarning.fadeOut(150).html("");
    var errMessage1 = "Sorry, there are no performances for the PLACES you have selected", errMessage2 = "Sorry, there are no performances for the TIMES you have selected", errMessage3 = "Sorry, there are no performances for the DATES you have selected", theStartParsed = moment(theStart, "YYYY-MM-DD").subtract(1, "days").format("ll"), theEndParsed = moment(theEnd, "YYYY-MM-DD").add(1, "days").format("ll");
    if (theCheckbox) {
        var theCategory = theCheckbox.attr("name"), theBox = theCheckbox.attr("id"), checked = theCheckbox.is(":checked");
        for (var theKey in plActiveFilters) theKey == theCategory && (checked ? plActiveFilters[theKey].push(theBox) : plActiveFilters[theKey].splice($.inArray(theBox, plActiveFilters[theKey]), 1));
    }
    plLoader.fadeIn(200);
    for (var theFilters = "", i = 0; i < plActiveFilters.where.length; i++) theFilters += "[data-where='" + plActiveFilters.where[i] + "']", 
    i < plActiveFilters.where.length - 1 && (theFilters += ",");
    var firstFilters = plFullList.filter(theFilters);
    plFullList.fadeOut(200), apHeaders.fadeOut(200), theFilters = "";
    for (var i = 0; i < plActiveFilters.time.length; i++) theFilters += "[data-time='" + plActiveFilters.time[i] + "']", 
    i < plActiveFilters.time.length - 1 && (theFilters += ",");
    var secondFilters = firstFilters.filter(theFilters);
    setTimeout(function() {
        var filterCount = 0;
        secondFilters.each(function() {
            var thePerf = $(this);
            moment(thePerf.data("when"), "LL").isBetween(theStartParsed, theEndParsed) && (thePerf.show(), 
            filterCount++, apHeaders.length > 0 && apHeaders.each(function() {
                var $this = $(this);
                $this.prop("id") == thePerf.data("title") && $this.show();
            }));
        }), firstFilters.length < 1 ? plWarning.fadeIn(150).html(errMessage1) : secondFilters.length < 1 ? plWarning.fadeIn(150).html(errMessage2) : 0 == filterCount && plWarning.fadeIn(150).html(errMessage3), 
        plLoader.fadeOut(150), perfList.simplebar("recalculate");
    }, 700);
}

function compareMilli(a, b) {
    return a.milli < b.milli ? -1 : a.milli > b.milli ? 1 : 0;
}

function openQuestion(activeNumber) {
    questionHolder.addClass("active"), closeQuestion();
    var currentQ = $(woBtns[activeNumber]), theFilters = currentQ.find(".filters");
    currentQ.addClass("active"), mediumScreen > screenWidth && (theFilters.css({
        height: "",
        opacity: "",
        transform: ""
    }), TweenLite.from(theFilters, .6, {
        height: 0,
        opacity: 0,
        onComplete: function() {
            theFilters.css({
                height: "",
                opacity: "",
                transform: ""
            }), $("html, body").animate({
                scrollTop: currentQ.offset().top - 90
            }, 500);
        }
    })), woActiveQ = activeNumber;
}

function closeQuestion() {
    var activeNumber, myQuestion = $(".wo-q-item.active"), mySelections = myQuestion.find(".selection .selected"), selIndicator = myQuestion.find(".wo-showing"), currentFilters = myQuestion.find(".filters");
    if ($(".wo-q-item").each(function(i) {
        $(this).hasClass("active") && (activeNumber = i);
    }), mySelections.length > 0 ? (selIndicator.html(""), mySelections.each(function(i) {
        var theChoice = "<span class='choice'>" + $(this).siblings("label").html() + "</span>";
        selIndicator.append(theChoice);
    }), selIndicator.addClass("chosen")) : "undefined" != typeof startDateSel && myQuestion.hasClass("wo-when") ? selIndicator.html("<span class='choice'>" + startDateSel + " - " + endDateSel + "</span>").addClass("chosen") : selIndicator.html(woShowingDefault[activeNumber]).removeClass("chosen"), 
    mediumScreen > screenWidth && TweenLite.to(currentFilters, .3, {
        height: 0,
        opacity: 0,
        onComplete: function() {
            myQuestion.removeClass("active");
        }
    }), woSearchOpen.hasClass("active")) {
        woSearchOpen.removeClass("active");
        var theBox = woSearchOpen.find(".form-holder");
        TweenLite.to(theBox, .4, {
            height: 0,
            "margin-top": 0,
            "margin-bottom": 0,
            opacity: 0,
            onComplete: function() {
                theBox.css({
                    height: ""
                });
            }
        });
    }
}

function gimmeGo(theThing) {
    if (1 == theThing) thePage.stop().scrollTo($(".wo-results"), {
        duration: 1e3,
        offsetTop: headerHeight
    }); else if (2 == theThing) thePage.stop().scrollTo($("#SelectedLevel"), {
        duration: 1e3,
        offsetTop: headerHeight
    }); else {
        var $theThing = $(theThing), myTopPos = $theThing.offset().top;
        $("body, html").stop().animate({
            scrollTop: myTopPos - headerHeight
        }, 1e3);
    }
}

function clickTheGrid(event) {
    function clearGrid() {
        TweenLite.to(clonedDetails, .2, {
            opacity: 0
        }), clonedGI.css({
            overflow: "hidden",
            "min-height": "none"
        }), TweenLite.to(clonedGI, .3, {
            delay: .15,
            height: 0,
            width: 0,
            opacity: 0
        }), TweenLite.to(gridFocusOuter, .3, {
            delay: .35,
            opacity: 0,
            height: gridFocusOuterHeight,
            onComplete: function() {
                gridFocusOuter.empty().removeClass("active").css({
                    height: ""
                });
            }
        });
    }
    "undefined" != typeof gridFocusOuter && gridFocusOuter.empty().removeClass("active");
    var thingy = event.data.theGridItem, thingyPos = thingy.position(), thingyTop = thingyPos.top, thingyLeft = thingyPos.left, thingyWidth = thingy.width(), thingyHeight = thingy.height();
    woActiveItem = thingy, gridFocusOuter = thingy.siblings(".active-gi-holder").addClass("active"), 
    TweenLite.to(gridFocusOuter, .5, {
        opacity: 1
    }), gridFocusOuterHeight = gridFocusOuter.height();
    var clonedGI = thingy.clone().addClass("active").appendTo(gridFocusOuter).css({
        "min-height": thingyHeight
    }), clonedGIHeight = clonedGI.height(), clonedGIWidth = clonedGI.width(), clonedGIMainImg = clonedGI.find(".main-image-holder"), clonedTitleCopy = clonedGI.find(".title-copy"), thingTitleCopyheight = thingy.find(".title-copy").height(), thingTitleBkgrndMarg = thingy.find(".gi-title-bkgrnd").css("margin-top"), clonedTitleBkgrnd = clonedGI.find(".gi-title-bkgrnd").css({
        "margin-top": thingTitleBkgrndMarg
    }), clonedDetails = clonedGI.find(".gi-details");
    clonedGI.css({
        height: thingyHeight,
        width: thingyWidth
    }), screenWidth >= smadiumScreen && largeScreen > screenWidth && thingyLeft > 0 ? (clonedGI.css({
        right: 0
    }), TweenLite.to(clonedGIMainImg, .4, {
        delay: .3,
        height: "147px",
        opacity: 0
    }), TweenLite.from(clonedTitleCopy, .4, {
        delay: .3,
        height: thingTitleCopyheight
    }), TweenLite.to(clonedTitleBkgrnd, .4, {
        delay: .3,
        "margin-top": "16px"
    }), TweenLite.to(clonedGI, .4, {
        delay: .3,
        height: clonedGIHeight,
        onComplete: function() {
            clonedDetails.css({
                display: "block"
            });
        }
    }), TweenLite.to(clonedGI, .4, {
        delay: .7,
        width: clonedGIWidth,
        onComplete: function() {
            clonedGI.css({
                height: "auto",
                width: ""
            });
        }
    }), TweenLite.to(clonedDetails, .4, {
        delay: 1,
        opacity: 1
    })) : screenWidth >= largeScreen && thingyLeft > gridFocusOuter.width() / 2 ? (clonedGI.css({
        right: 0
    }), TweenLite.to(clonedGIMainImg, .4, {
        delay: .3,
        height: "147px",
        opacity: 0
    }), TweenLite.from(clonedTitleCopy, .4, {
        delay: .3,
        height: thingTitleCopyheight
    }), TweenLite.to(clonedTitleBkgrnd, .4, {
        delay: .3,
        "margin-top": "16px"
    }), TweenLite.to(clonedGI, .4, {
        delay: .3,
        height: clonedGIHeight,
        onComplete: function() {
            clonedDetails.css({
                display: "block"
            });
        }
    }), TweenLite.to(clonedGI, .4, {
        delay: .7,
        width: clonedGIWidth,
        onComplete: function() {
            clonedGI.css({
                height: "auto",
                width: ""
            });
        }
    }), TweenLite.to(clonedDetails, .4, {
        delay: 1,
        opacity: 1
    })) : (clonedGI.css({
        left: thingyLeft
    }), TweenLite.to(clonedGIMainImg, .4, {
        delay: .3,
        height: "147px",
        opacity: 0
    }), TweenLite.from(clonedTitleCopy, .4, {
        delay: .3,
        height: thingTitleCopyheight
    }), TweenLite.to(clonedTitleBkgrnd, .4, {
        delay: .3,
        "margin-top": "16px"
    }), TweenLite.to(clonedGI, .4, {
        delay: .3,
        height: clonedGIHeight,
        onComplete: function() {
            clonedDetails.css({
                display: "block"
            });
        }
    }), TweenLite.to(clonedGI, .4, {
        delay: .7,
        width: clonedGIWidth,
        onComplete: function() {
            clonedGI.css({
                height: "auto",
                width: ""
            });
        }
    }), TweenLite.to(clonedDetails, .4, {
        delay: 1,
        opacity: 1
    })), gridFocusOuterHeight < clonedGIHeight && TweenLite.to(gridFocusOuter, .4, {
        delay: .3,
        height: clonedGIHeight
    }), thingyHeight + thingyTop > gridFocusOuterHeight - 10 && thingyTop > 10 ? clonedGI.css({
        bottom: 0
    }) : clonedGI.css({
        top: thingyTop
    }), clonedGI.click(function(e) {
        e.stopPropagation();
    }), gridFocusOuter.click(function(e) {
        clearGrid();
    });
    var lessButton = gridFocusOuter.find(".less-button");
    lessButton.click(function(e) {
        e.preventDefault(), clearGrid();
    });
}

function updateLink() {
    var whatLink, whereLink, startLink, endLink, viewLink, newLink = "/whats-on/filter/";
    whatLink = selectedChoicesObj.what.length > 0 ? selectedChoicesObj.what.join("-") : "any", 
    whereLink = selectedChoicesObj.where.length > 0 ? selectedChoicesObj.where.join("-") : "any", 
    startLink = selectedChoicesObj.when[0], endLink = selectedChoicesObj.when[1], viewLink = selectedChoicesObj.view, 
    newLink += "what/" + whatLink + "/where/" + whereLink + "/when/" + startLink + "/" + endLink + "/view/" + viewLink, 
    goButtons.attr("href", newLink);
}

function ShowView(a) {
    var b, c;
    switch (b = a.replace("wo-", "")) {
      case "grid-view":
        c = "grid";
        break;

      case "calendar-view":
        c = "calendar";
    }
    selectedChoicesObj.view = c, updateLink(), $("#" + b).siblings(".view").fadeOut("slow", function() {
        $("#" + b).fadeIn("slow"), $("#" + a).siblings(".view-btn").removeClass("selected"), 
        $("#" + a).addClass("selected"), gimmeGo($("#" + b));
    });
}

function openDayAnimation() {
    var activePerfs, closeDay, allPerfsCloseDay, perfSum = expandedModule.find(".perf-sum"), perfSumHeight = perfSum.height(), myPerfs = expandedModule.find(".ex-mod-inner"), myWeek = expandedModule.parent(".week");
    screenWidth >= mediumScreen ? (activePerfs = myPerfs.clone().appendTo(myWeek).prop("id", "openPerfs").addClass("cloned"), 
    console.log("cloning")) : activePerfs = myPerfs.prop("id", "openPerfs"), expandedModule.addClass("selected"), 
    expandedModule.attr("id", "selectedDay"), closeDay = expandedModule.find(".more-button + .less-button"), 
    allPerfsCloseDay = activePerfs.find(".less-button"), TweenLite.from(perfSum, .2, {
        height: perfSumHeight,
        opacity: 1,
        onComplete: function() {
            perfSum.css({
                opacity: "",
                height: ""
            });
        }
    }), activePerfs.show(), TweenLite.from(activePerfs, 1, {
        height: 0,
        opacity: 0,
        ease: Power2.easeInOut,
        onComplete: function() {
            activePerfs.css({
                height: "",
                opacity: ""
            }), closeDay.click(function(e) {
                dsOffset = expandedModule.offset().top, expandedModule = "undefined", collapseInnerDetails(e);
            }), allPerfsCloseDay.click(function(e) {
                dsOffset = expandedModule.offset().top, expandedModule = "undefined", collapseInnerDetails(e);
            });
        }
    });
}

function closeDayAnimation() {
    var openPerfs = $("#openPerfs"), openDay = (openPerfs.height(), $("#selectedDay")), openPerfSum = $("#selectedDay .perf-sum");
    openPerfs.attr("id", ""), TweenLite.to(openPerfs, .4, {
        height: 0,
        opacity: 0,
        onComplete: function() {
            openPerfs.hide(), screenWidth >= mediumScreen ? openPerfs.remove() : openPerfs.css({
                height: "",
                opacity: ""
            });
        }
    }), openDay.attr("id", "").removeClass("selected"), TweenLite.from(openPerfSum, .4, {
        height: 0,
        opacity: 0,
        onComplete: function() {
            openPerfSum.css({
                opacity: "",
                height: ""
            });
        }
    }), ("undefined" == typeof expandedModule || "undefined" == expandedModule) && TweenLite.to(theBody, .1, {
        scrollTop: dsOffset - 90
    });
}

$(function() {
    if ($(".cast-profile").length < 1) return !1;
    $(".cast-profile").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: !1,
        arrows: !0,
        fade: !0,
        touchThreshold: 5,
        adaptiveHeight: !0
    }).on("beforeChange", function(event, slick, currentSlide, nextSlide) {
        $(".cast-thumbs .active-thumb").removeClass("active-thumb");
        var $current = $(".cast-thumbs .slick-current"), $prev = $current.prevAll('[data-thumb="' + nextSlide + '"]').first(), $next = $current.nextAll('[data-thumb="' + nextSlide + '"]').first();
        if ($prev.length > 0 && $next.length > 0) {
            var prevDistance = $current.index() - $prev.index(), nextDistance = $next.index() - $current.index();
            nextDistance > prevDistance ? ($(".cast-thumbs").slick("slickGoTo", $prev.attr("data-slick-index")), 
            $('.cast-thumbs [data-thumb="' + nextSlide + '"]').addClass("active-thumb")) : ($(".cast-thumbs").slick("slickGoTo", $next.attr("data-slick-index")), 
            $('.cast-thumbs [data-thumb="' + nextSlide + '"]').addClass("active-thumb"));
        } else if ($prev.length > 0) $(".cast-thumbs").slick("slickGoTo", $prev.attr("data-slick-index")), 
        $('.cast-thumbs [data-thumb="' + nextSlide + '"]').addClass("active-thumb"); else if ($next.length > 0) $(".cast-thumbs").slick("slickGoTo", $next.attr("data-slick-index")), 
        $('.cast-thumbs [data-thumb="' + nextSlide + '"]').addClass("active-thumb"); else {
            var $thumbs = $('.cast-thumbs [data-thumb="' + nextSlide + '"]').closest(".slick-slide"), shortestDistance = null;
            $thumbs.each(function() {
                var distance = Math.abs($current.index() - $(this).index());
                (null == shortestDistance || shortestDistance > distance) && (shortestDistance = distance, 
                shortestIndex = $(this).attr("data-slick-index"));
            }), $('.cast-thumbs [data-thumb="' + nextSlide + '"]').addClass("active-thumb"), 
            $(".cast-thumbs").slick("slickGoTo", shortestIndex);
        }
    }).on("afterChange", function(event, slick, currentSlide) {
        $img = $(this).find(".slick-active figure img"), $img.length > 0 ? $(".cast-profile .slick-prev, .cast-profile .slick-next").css("top", $img.first().height() / 2) : $(".cast-profile .slick-prev, .cast-profile .slick-next").css("top", $(this).find(".slick-active").height() / 2);
    }), $(".carousel-prev").click(function() {
        $(".cast-profile").slick("slickPrev");
    }), $(".carousel-next").click(function() {
        $(".cast-profile").slick("slickNext");
    }), $(".close-cast-profile").click(function() {
        $(".cast-profile-container").slideUp(500, function() {
            $(this).css("display", "block").css("position", "absolute");
        }), $(".cast-thumb.active-thumb").removeClass("active-thumb");
    });
    var i = 0;
    $(".cast-thumbs").children().each(function() {
        $(this).attr("data-thumb", i), i++;
    }), $(".cast-thumbs").on("click", ".cast-thumb", function(e) {
        var thumb = $(this).data("thumb");
        $(".active-thumb").removeClass("active-thumb"), $('[data-thumb="' + thumb + '"]').addClass("active-thumb"), 
        $(".cast-profile").slick("slickGoTo", thumb), $(".cast-profile").resize(), $(".cast-profile .slick-list").height($(".cast-profile .slick-current").height()), 
        $container = $(".cast-profile-container"), "absolute" == $container.css("position") && $container.css("position", "relative").css("display", "none"), 
        $container.slideDown(400, function() {
            $(".slick-current").css("z-index", "0");
        }), $(".cast-creative-widget").focus(), $("body, html").scrollTop(Math.floor($(".cast-creative-widget").offset().top - $(".header-container .header-top").height()));
    }), $(".cast-thumbs").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: !1,
        focusOnSelect: !0,
        touchThreshold: 50,
        speed: 750,
        rows: $(window).width() >= 1440 ? 2 : 1,
        mobileFirst: !0,
        responsive: [ {
            breakpoint: 0,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 1440,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1
            }
        } ]
    }), $(".creative-carousel").slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        dots: !1,
        focusOnSelect: !0,
        touchThreshold: 50,
        speed: 750,
        mobileFirst: !0,
        responsive: [ {
            breakpoint: 0,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 790,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 1024,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 1440,
            settings: {
                slidesToShow: 6,
                slidesToScroll: 1
            }
        } ]
    }), hashChange = function() {
        var hash = location.hash.substr(1);
        "" !== hash && ($slide = $('[data-hash="' + hash + '"]'), $slide.length > 0 && $slide.first().trigger("click"));
    }, hashChange(), window.onhashchange = function() {
        hashChange();
    }, $(".slick-slide div:last-child .cast-thumb").addClass("odd-thumb");
}), $(function() {
    $(".gallery-widget").length > 0 && $(".gallery-widget").each(function(j) {
        $this = $(this).attr("id", "galleryWidget" + j);
        var theThumbsID = "galleryThumbs" + j, thePhotosID = "galleryPhotos" + j;
        $this.find(".gallery-thumbs").attr("id", theThumbsID), $this.find(".gallery-photos").attr("id", thePhotosID), 
        $this.find(".gallery-thumbs-container").attr("id", theThumbsID + "cont"), $this.find(".gallery-photo-container").attr("id", thePhotosID + "cont"), 
        gallerySetup($this, theThumbsID, thePhotosID);
    });
});

var theBody = $("body"), thePage = $("body, html"), mediaHolder = $("#mediaHolder"), largeVideoHolder = $(".large-video-holder"), largeIframe = $(".large-video-holder iframe"), smlVideoHolder = $(".small-video-holder"), smlIframe = $(".small-video-holder iframe"), homeVideoHolder = $("#homeVideoHolder"), homeVidIframe = $("#homeVideoHolder iframe"), homeVideoCover = $("#videoCover img"), quickLinksBtn = $(".ql-head"), quicklinksCont = $(".ql-body"), smallScreen = 480, smadiumScreen = 600, mediumScreen = 790, largeScreen = 1024, xlargeScreen = 1440, screenWidth = window.innerWidth, inCinemas = $("#inCinemasHolder"), showHidden = $(".show-hidden"), introCopy = $("#playIntroCopy p"), prBooking = $("#prBooking"), copyInner = $("#copyInner"), moveIntro, movePRBooking, quoteHolder = $(".quote-holder:last-of-type"), floatNavOn = $("#floatNavOnBtn"), floatNavOff = $("#floatNavOffBtn"), floatNav = $("#floatNavList"), floatSubNav = $("#floatingMenu .sub-nav"), nextPerf = $("#nextPerf"), videoCover = $("#videoCover"), playVideo = $("#playVideo"), mainImage = $(".play-hub .main-image"), sponLogo = $("#spHolder"), moveSponsor, plFullList = $(".performance-list-item"), plChecks = $(".pl-filter .k-checkbox"), plActiveFilters = {
    where: [ "stratford", "london" ],
    time: [ "matinee", "evening" ],
    when: []
}, plFilterGroups = $(".pl-filter:not(.pl-calendar):not(.pl-promocode)"), perfList = $("#perfList"), plWarning = $("#pl-warning"), plLoader = $(".loader-outer"), apHeaders = $(".pl-list h2"), theWOHeader = $(".whatson header"), theWOHeaderOrigHeight = theWOHeader.height(), woBtns = $(".wo-q-item"), woOpenTransport = $(".wo-transport-lrg .more-button"), woCloseTransport = $(".wo-transport-lrg .less-button"), woSearchOpen = $("#woSearchOpen"), woSearchClose = woSearchOpen.find(".less-button"), woSearchForm = woSearchOpen.find(".form-holder"), woSearchSubmit = $("#woSearchBtn"), woSearchBar = $("#woSearchBar"), questionHolder = $(".wo-questions"), woHeader = $(".wo-header"), woQCloseBtns = $(".wo-q-item .less-button"), woLeft = $(".wo-left"), woRight = $(".wo-right"), woActiveQ, selects = $(".selection input"), ieCalDropdown = $(".cal-ie-fallback #ie-month-select"), goButtons = $(".go-button"), resetButtons = $(".reset-filters"), selectedChoices = [], selectedChoicesObj = {
    what: [],
    where: [],
    when: [ "any", "any" ],
    view: "grid"
}, woShowingDefault = [ "Plays, Events, Activities&hellip;", "Stratford-upon-Avon, London, On Tour&hellip;", "This Week, Next Week, Next Month&hellip;" ], woActiveItem, gridItems = $(".wo-grid-item"), gridFocusOuter, rightNow = moment().format("LL"), yesterday = moment().subtract(1, "days").format("LL"), nextWeek = moment().add(7, "days").format("LL"), lastDay = moment().add(4, "months").format("LL"), startDateSel, endDateSel, dsOffset, woChangeSearch = $("#woChangeSearch"), breakpoint = {}, globalHeader = $("#globalHeader"), globalNav = $("#globalNav"), globalHeadertop = $(".header-top"), loginSearch = $(".login-search"), topLevelLogin = $("#MobileTopLevelLogin .login-link"), theMenuButton = $("#menuButton"), theBurger = $("#burgerHolder"), mainNav = $("nav"), mainNavLis = $(".top-level li"), mainNavBtns = $(".top-level li a.top-level-link:not(.nodrop)"), subNavItems = $(".subnav-item"), subNavBkgrnd = $(".subnav-bkgrnd"), subNavClose = $("#subNavCloseBtn"), didScroll, lastScrollTop = 0, delta = 5, headerHeight = globalNav.outerHeight() + globalHeadertop.outerHeight(), fullHeaderHeight = headerHeight + $(".play-hub header").outerHeight() - 40, gFooterOffset = $(".footer-container").offset(), gFooterTop = gFooterOffset.top, windowHeight = window.innerHeight, floatingMenu = $("#floatingMenu"), globalSearch = $(".global-search-box"), globalSearchOpen = $(".search-open"), globalSearchClose = $(".global-search-box .close-btn"), globalSearchInput = $("#globalSearchInput"), globalSearchBtn = $("#globalSearchButton"), shopSearch = $(".shop-header input[type=text]"), shopSearchGo = $(".shop-header #shopSearch"), infoPopUp = $(".info-popup"), infoClose = $(".info-popup .close-btn"), sendMessage = $("#send-message"), contactFormHolder = $("#contact-form-holder"), galleryIframe = $(".gallery-photo-container iframe"), searchAgain = $(".search-again"), resourceForm = $(".resource-form"), resourcePlay = $(".resource-form #resPlaySelect"), resourceType = $(".resource-form #resType input[type=checkbox]"), resourceAge = $(".resource-form #resAge input[type=checkbox]"), resBankFilters = {
    play: [],
    type: [],
    age: []
}, allExMods = $(".ex-mod"), expandedModule, horzScollers = $(".hs-outer"), hsTransport = $(".hs-transport"), horzScollerInners = [];

$(function() {
    function hasScrolled() {
        var st = $(this).scrollTop();
        Math.abs(lastScrollTop - st) <= delta || (window.innerWidth >= largeScreen && (st > lastScrollTop && st > headerHeight - 20 && !theMenuButton.hasClass("mb-active") ? (globalNav.addClass("nav-up").removeClass("nav-down"), 
        theMenuButton.addClass("mb-active")) : lastScrollTop > st && headerHeight + 100 > st ? (globalNav.removeClass("nav-up").addClass("nav-down"), 
        theMenuButton.removeClass("mb-active"), theBurger.removeClass("active")) : lastScrollTop > st && lastScrollTop - st ? (globalNav.removeClass("nav-up").addClass("nav-down"), 
        theBurger.addClass("active")) : st > headerHeight && theBurger.hasClass("active") && (globalNav.addClass("nav-up").removeClass("nav-down"), 
        theBurger.removeClass("active"))), st > fullHeaderHeight && gFooterTop > st + windowHeight && !floatingMenu.hasClass("active") ? floatingMenu.addClass("active") : (fullHeaderHeight > st || st + windowHeight > gFooterTop) && floatingMenu.hasClass("active") && (floatingMenu.removeClass("active"), 
        floatNav.removeClass("active"), floatNavOn.removeClass("open"), floatNavOff.addClass("closed"), 
        nextPerf.removeClass("active")), lastScrollTop = st);
    }
    function homeVidEnd(event) {
        0 == event.data && ($("#homeVideoHolder #videoCover").fadeIn(200), $(".hero-copy .intro-btn").css({
            display: "inline-block"
        }), $(".hero-copy p").css({
            display: "none"
        }), $(".hero-copy").fadeIn(200), playVideo.fadeIn(200));
    }
    if ($(window).resize(function() {
        screenWidth = window.innerWidth, subNavBkgrnd.removeClass("vis"), subNavItems.removeClass("vis"), 
        theBody.removeClass("no-scroll"), largeScreen > screenWidth ? (headerHeight = globalHeadertop.outerHeight(), 
        theBurger.hasClass("active") && (theBurger.removeClass("active"), mainNavBtns.removeClass("vis"), 
        mainNav.removeClass("nav-active").addClass("nav-hide"))) : (headerHeight = globalNav.outerHeight() + globalHeadertop.outerHeight(), 
        theBurger.hasClass("active") && mainNav.removeClass("nav-active")), videoResize(largeVideoHolder, smlVideoHolder), 
        "undefined" != typeof introCopy && introCopy.length > 0 && ("undefined" != typeof prBooking && prBooking.length > 0 ? (window.innerWidth >= mediumScreen && window.innerWidth < xlargeScreen && $("#moveIntro").length < 1 ? (moveIntro = introCopy.clone().removeClass("ongray-intro").addClass("intro-copy").prop("id", "moveIntro").prependTo(copyInner), 
        introCopy.css({
            display: "none"
        }), introCopy.parent(".play-intro-copy").css({
            position: "",
            bottom: ""
        })) : window.innerWidth < mediumScreen && $("#moveIntro").length > 0 ? (moveIntro.remove(), 
        introCopy.css({
            display: ""
        }), introCopy.parent(".play-intro-copy").css({
            position: "",
            bottom: ""
        })) : window.innerWidth >= xlargeScreen && $("#moveIntro").length > 0 ? (moveIntro.remove(), 
        introCopy.css({
            display: ""
        }), introCopy.parent(".play-intro-copy").css({
            position: "absolute",
            bottom: "32px"
        })) : window.innerWidth >= xlargeScreen && introCopy.parent(".play-intro-copy").css({
            position: "absolute",
            bottom: "32px"
        }), window.innerWidth >= mediumScreen && window.innerWidth < largeScreen && $("#movePRBooking").length < 1 ? movePRBooking = prBooking.clone().removeClass("ongray-intro").addClass("intro-copy").prop("id", "movePRBooking").prependTo(copyInner) : !(window.innerWidth >= mediumScreen && window.innerWidth < largeScreen) && $("#movePRBooking").length > 0 && movePRBooking.remove()) : window.innerWidth >= mediumScreen && window.innerWidth < largeScreen && $("#moveIntro").length < 1 ? moveIntro = introCopy.clone().removeClass("ongray-intro").addClass("intro-copy").prop("id", "moveIntro").prependTo(copyInner) : !(window.innerWidth >= mediumScreen && window.innerWidth < largeScreen) && $("#moveIntro").length > 0 && moveIntro.remove()), 
        "undefined" != typeof mainImage && mainImage.length > 0 && "undefined" != typeof sponLogo && sponLogo.length > 0 && (window.innerWidth < mediumScreen && sponLogo.hasClass("first-level") && $(".move-sponsor").length < 1 ? moveSponsor = sponLogo.clone().addClass("move-sponsor").insertAfter(mainImage) : window.innerWidth >= mediumScreen && $(".move-sponsor").length > 0 && moveSponsor.remove());
        var playHubHeaderHeight = $(".play-hub header").outerHeight();
        if (windowHeight = window.innerHeight, gFooterOffset = $(".footer-container").offset(), 
        gFooterTop = gFooterOffset.top, fullHeaderHeight = headerHeight + playHubHeaderHeight - 150, 
        "undefined" != typeof woActiveItem && woActiveItem.length > 0) {
            var woActivePos = woActiveItem.position(), woActiveWidth = woActiveItem.width(), woActiveHeight = woActiveItem.height(), focusItem = gridFocusOuter.children(".wo-grid-item").css({
                "min-height": woActiveHeight
            }), gridFocusOuterHeight = gridFocusOuter.height();
            screenWidth >= smadiumScreen && largeScreen > screenWidth && woActivePos.left > 0 ? focusItem.css({
                left: 0
            }) : screenWidth >= largeScreen && woActivePos.left > gridFocusOuter.width() / 2 ? focusItem.css({
                left: woActiveWidth
            }) : focusItem.css({
                left: woActivePos.left
            }), woActiveHeight + woActivePos.top > gridFocusOuterHeight - 10 ? focusItem.css({
                bottom: 0
            }) : focusItem.css({
                top: woActivePos.top
            });
        }
        if ("undefined" != typeof questionHolder && questionHolder.length > 0 && window.innerWidth >= mediumScreen && (questionHolder.find(".filters").attr("style", ""), 
        woBtns.each(function(i) {
            $(this).removeClass("active"), $(this).find(".wo-showing").html(woShowingDefault[i]);
        })), "undefined" != typeof perfList && perfList.length > 0 && perfList.simplebar("recalculate"), 
        "undefined" != typeof $("#openPerfs") && $("#openPerfs").length > 0) {
            var openPerfs = $("#openPerfs"), openWeek = openPerfs.parents(".week"), openDayPerfs = ($(".day.selected"), 
            $(".day.selected .all-perfs"));
            if (mediumScreen > screenWidth && openPerfs.hasClass("cloned")) openPerfs.remove(), 
            openDayPerfs.show().prop("id", "openPerfs"); else if (screenWidth >= mediumScreen && !openPerfs.hasClass("cloned")) {
                openPerfs.clone().appendTo(openWeek).addClass("cloned").css({
                    height: ""
                });
                openPerfs.hide().css({
                    height: "",
                    opacity: ""
                }).prop("id", "");
            }
        }
    }).resize(), Modernizr.addTest("mix-blend-mode", function() {
        return Modernizr.testProp("mixBlendMode");
    }), -1 != navigator.userAgent.indexOf("Safari") && -1 == navigator.userAgent.indexOf("Chrome") && $(".bkgrnd-strip.yellow-color").addClass("safari"), 
    quickLinksBtn.click(function(e) {
        mediumScreen > screenWidth && !quickLinksBtn.hasClass("opened") && (quicklinksCont.css("height", "auto"), 
        TweenLite.from(quicklinksCont, .2, {
            height: 0,
            opacity: 0,
            ease: Circ.easeOut
        }), quickLinksBtn.addClass("opened"));
    }), showHidden.each(function() {
        var $thisHid = $(this);
        $thisHid.click(function(e) {
            e.preventDefault(), showIt(e);
        });
    }), theMenuButton.click(function(e) {
        theBurger.toggleClass("active"), subNavItems.removeClass("vis"), window.innerWidth < largeScreen ? (mainNav.toggleClass("nav-active").toggleClass("nav-hide"), 
        theBody.toggleClass("noscroll")) : window.innerWidth >= largeScreen && (lastScrollTop > headerHeight - 20 ? globalNav.toggleClass("nav-up").toggleClass("nav-down") : theMenuButton.toggleClass("mb-active"), 
        subNavBkgrnd.hasClass("vis") && (subNavBkgrnd.removeClass("vis").addClass("vis-fade"), 
        setTimeout(function() {
            subNavBkgrnd.removeClass("vis-fade"), theBody.removeClass("noscroll");
        }, 150)));
    }), navBtns(mainNavBtns, subNavItems), $(window).scroll(function(event) {
        didScroll = !0;
    }), setInterval(function() {
        didScroll && (hasScrolled(), didScroll = !1);
    }, 250), void 0 !== typeof infoPopUp && infoPopUp.length > 0) {
        var infoPHeight = [ 8 ];
        infoPopUp.each(function(i) {
            var $this = $(this);
            if (!$this.hasClass("here")) {
                var theTime = 800 + 350 * i, newHeight = infoPHeight.reduce(arrayAdd, 0) + 8;
                setTimeout(function() {
                    $this.css({
                        visibility: "visible"
                    }).addClass("here"), TweenLite.to($this, .4, {
                        bottom: newHeight,
                        opacity: 1,
                        ease: Circ.easeOut
                    });
                }, theTime), infoPHeight.push($this.height() + 8);
            }
        });
    }
    if (infoClose.each(function(i) {
        var $this = $(this);
        $this.click(function() {
            closeInfo(i);
        });
    }), $(".info-copy .more-button").click(function(e) {
        e.preventDefault();
        var $this = $(this);
        $this.siblings(".hide").removeClass("hide"), $this.hide();
    }), globalSearchOpen.each(function(i) {
        $this = $(this), $this.click(function(e) {
            e.preventDefault(), globalSearchShow();
        });
    }), globalSearchClose.click(function(e) {
        globalSearchHide();
    }), globalSearchInput.change(function() {
        var str = globalSearchInput.val();
        str = str.replace(/\s+/g, " ").toLowerCase(), globalSearchBtn.prop("href", "/search?q=" + str);
    }), globalSearchBtn.click(function(e) {
        e.preventDefault;
        var str = globalSearchInput.val();
        str = str.replace(/\s+/g, " ").toLowerCase(), globalSearchBtn.prop("href", "/search?q=" + str), 
        window.location.href = "/search?q=" + str;
    }), globalSearchInput.on("keypress", function(e) {
        if (13 == e.keyCode) {
            var str = globalSearchInput.val();
            return str = str.replace(/\s+/g, " ").toLowerCase(), globalSearchBtn.prop("href", "/search?q=" + str), 
            window.location.href = "/search?q=" + str, !1;
        }
    }), shopSearch.change(function() {
        var str = shopSearch.val();
        str = str.replace(/\s/g, " ").toLowerCase(), shopSearchGo.prop("href", "/shop/search?q=" + str);
    }), shopSearchGo.click(function(e) {
        e.preventDefault;
        var str = shopSearch.val();
        str = str.replace(/\s+/g, " ").toLowerCase(), shopSearchGo.prop("href", "/shop/search?q=" + str), 
        window.location.href = "/shop/search?q=" + str;
    }), shopSearch.on("keypress", function(e) {
        if (13 == e.keyCode) {
            var str = shopSearch.val();
            return str = str.replace(/\s+/g, " ").toLowerCase(), shopSearchGo.prop("href", "/shop/search?q=" + str), 
            window.location.href = "/shop/search?q=" + str, !1;
        }
    }), $("#membDate").length > 0 && void 0 != typeof pikadayResponsive) {
        var todayPlusSeven = new Date();
        todayPlusSeven.setDate(todayPlusSeven.getDate() + 7);
        new pikadayResponsive(document.getElementById("membDate"), {
            format: "D MMM YY",
            outputFormat: "ll",
            classes: "member-start-date",
            pikadayOptions: {
                minDate: todayPlusSeven
            }
        });
        (navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPhone/i)) && $("#membDate").change(function() {
            var theActualDate = $("#membDate").attr("value");
            moment(theActualDate, "ll").isBefore(todayPlusSeven) ? ($(".date-validator").fadeIn(200), 
            $("#membDate").siblings("input[type=text]").addClass("error")) : ($(".date-validator").hide(), 
            $("#membDate").siblings("input[type=text]").removeClass("error"));
        });
    }
    if ($(".dobField").length > 0 && void 0 != typeof pikadayResponsive) {
        var dobSet, dobDefault, dobStart = new Date(), dobEnd = new Date(), dobTenYears = new Date();
        dobStart.setYear(dobStart.getYear() - 100), dobTenYears.setYear(dobTenYears.getFullYear() - 10), 
        dobEnd.setYear(dobEnd.getFullYear() - 5), "" !== $(".dobField").prop("value") ? (dobSet = new Date($(".dobField").attr("value")), 
        dobDefault = !0) : (dobSet = dobEnd, dobDefault = !1);
        new pikadayResponsive($(".dobField"), {
            format: "ll",
            outputFormat: "YYYY-MM-DD",
            classes: "member-dob",
            pikadayOptions: {
                minDate: dobStart,
                maxDate: dobEnd,
                defaultDate: dobSet,
                setDefaultDate: dobDefault,
                yearRange: 100
            }
        });
        (navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPhone/i)) && $(".dobField").change(function() {
            var theActualDate = moment($(".dobField").prop("value"), "YYYY-MM-DD");
            theActualDate.isBefore(dobStart) ? ($(".form-error").hide(), $(".hundred-years").fadeIn(200), 
            $(".dobField").siblings("input[type=text]").addClass("error")) : theActualDate.isAfter(dobEnd) ? ($(".form-error").hide(), 
            $(".future").fadeIn(200), $(".dobField").siblings("input[type=text]").addClass("error")) : theActualDate.isAfter(dobTenYears) ? ($(".form-error").hide(), 
            $(".ten-years").fadeIn(200), $(".dobField").siblings("input[type=text]").addClass("error")) : ($(".form-error").hide(), 
            $(".dobField").siblings("input[type=text]").removeClass("error"));
        });
    }
    if (searchAgain.each(function(i) {
        $(this).click(function() {
            var theForm = $(this).siblings(".form-holder");
            theForm.show(), TweenLite.from(theForm, .4, {
                height: 0,
                opacity: 0
            }), TweenLite.to($(this), .2, {
                height: 0,
                opacity: 0,
                "padding-top": 0,
                "padding-bottom": 0
            });
        });
    }), resBankFilters.play = resourcePlay.val(), resourcePlay.change(function() {
        resBankFilters.play = $(this).val(), edResourceLink();
    }), resourceType.each(function() {
        var resTypeCopy = $(this).attr("id"), resTypeArr = resBankFilters.type;
        ("checked" == $(this).attr("checked") || $(this).is(":checked")) && resTypeArr.push(resTypeCopy), 
        edResourceLink(), $(this).change(function() {
            $.inArray(resTypeCopy, resTypeArr) > -1 ? resTypeArr.splice($.inArray(resTypeCopy, resTypeArr), 1) : resTypeArr.push(resTypeCopy), 
            edResourceLink();
        });
    }), resourceAge.each(function() {
        var resAgeCopy = $(this).attr("id"), resAgeArr = resBankFilters.age;
        ("checked" == $(this).attr("checked") || $(this).is(":checked")) && resAgeArr.push(resAgeCopy), 
        edResourceLink(), $(this).change(function() {
            $.inArray(resAgeCopy, resAgeArr) > -1 ? resAgeArr.splice($.inArray(resAgeCopy, resAgeArr), 1) : resAgeArr.push(resAgeCopy), 
            edResourceLink();
        });
    }), void 0 != typeof sendMessage && sendMessage.click(function(e) {
        e.preventDefault, contactFormHolder.show(), TweenLite.from(contactFormHolder, .4, {
            height: 0,
            opacity: 0
        }), TweenLite.to(sendMessage, .2, {
            height: 0,
            opacity: 0
        });
    }), woChangeSearch.click(function(e) {
        e.preventDefault(), gimmeGo(theWOHeader);
    }), woOpenTransport.click(function(e) {
        questionHolder.addClass("active");
    }), woCloseTransport.click(function(e) {
        questionHolder.removeClass("active");
    }), $("#woStart").length > 0 && void 0 !== typeof pikadayResponsive) {
        var startSetDate, endSetDate, finalDate = new Date(), SDisSet = !1, EDisSet = !1, woStartAttr = $("#woStart").attr("data-value"), woEndAttr = $("#woEnd").attr("data-value");
        finalDate.setDate(finalDate.getDate() + 540), "undefined" != typeof woStartAttr && woStartAttr !== !1 ? (startSetDate = new Date($("#woStart").attr("data-value")), 
        SDisSet = !0, selectedChoicesObj.when[0] = moment($("#woStart").attr("data-value")).format("YYYY-MM-DD"), 
        goButtons.each(function(i) {
            $(this).removeClass("inactive");
        }), resetButtons.each(function(i) {
            $(this).removeClass("inactive");
        }), updateLink()) : startSetDate = new Date(), "undefined" != typeof woEndAttr && woEndAttr !== !1 ? (endSetDate = new Date($("#woEnd").attr("data-value")), 
        EDisSet = !0, selectedChoicesObj.when[1] = moment($("#woEnd").attr("data-value")).format("YYYY-MM-DD"), 
        goButtons.each(function(i) {
            $(this).removeClass("inactive");
        }), resetButtons.each(function(i) {
            $(this).removeClass("inactive");
        })) : (endSetDate = startSetDate, endSetDate.setDate(endSetDate.getMonth() + 1));
        var startDate, endDate, firstRun = 0;
        if ($(".filters .calendar .text-fields").is(":visible")) {
            var updateStartDate = function() {
                startPicker.pikaday.setStartRange(startDate), endPicker.pikaday.setStartRange(startDate), 
                endPicker.pikaday.setMinDate(startDate), goButtons.each(function(i) {
                    $(this).removeClass("inactive");
                }), resetButtons.each(function(i) {
                    $(this).removeClass("inactive");
                });
            }, updateEndDate = function() {
                startPicker.pikaday.setEndRange(endDate), startPicker.pikaday.setMaxDate(endDate), 
                endPicker.pikaday.setEndRange(endDate), goButtons.each(function(i) {
                    $(this).removeClass("inactive");
                }), resetButtons.each(function(i) {
                    $(this).removeClass("inactive");
                });
            }, startPicker = new pikadayResponsive(document.getElementById("woStart"), {
                format: "D MMM YY",
                outputFormat: "ll",
                classes: "woStartDate",
                pikadayOptions: {
                    minDate: new Date(),
                    maxDate: finalDate,
                    defaultDate: startSetDate,
                    setDefaultDate: SDisSet,
                    onSelect: function() {
                        startDate = this.getDate(), firstRun > 0 && updateStartDate(), startDateSel = moment(startDate).format("d MMM YY"), 
                        selectedChoicesObj.when[0] = moment(startDate).format("YYYY-MM-DD"), SDisSet && (goButtons.each(function(i) {
                            $(this).removeClass("inactive");
                        }), resetButtons.each(function(i) {
                            $(this).removeClass("inactive");
                        })), updateLink();
                    }
                }
            }), endPicker = new pikadayResponsive(document.getElementById("woEnd"), {
                format: "D MMM YY",
                outputFormat: "ll",
                classes: "woEndDate",
                pikadayOptions: {
                    minDate: new Date(),
                    maxDate: finalDate,
                    defaultDate: endSetDate,
                    setDefaultDate: EDisSet,
                    onSelect: function() {
                        endDate = this.getDate(), firstRun > 0 && updateEndDate(), endDateSel = moment(endDate).format("d MMM YY"), 
                        selectedChoicesObj.when[1] = moment(endDate).format("YYYY-MM-DD"), EDisSet && (goButtons.each(function(i) {
                            $(this).removeClass("inactive");
                        }), resetButtons.each(function(i) {
                            $(this).removeClass("inactive");
                        })), updateLink();
                    }
                }
            });
            if (firstRun += 1, startPicker.pikaday) var _startDate = startPicker.pikaday.getDate(), _endDate = endPicker.pikaday.getDate();
        }
    }
    if (_startDate && (startDate = _startDate, updateStartDate()), _endDate && (endDate = _endDate, 
    updateEndDate()), woBtns.each(function(i) {
        $(this).click(function() {
            !$(this).hasClass("active") && mediumScreen > screenWidth ? openQuestion(i) : !questionHolder.hasClass("active") && screenWidth >= mediumScreen && questionHolder.addClass("active");
        });
    }), woSearchOpen.click(function(e) {
        e.stopPropagation();
        var theSearch = $(this), theBox = theSearch.find(".form-holder");
        theSearch.hasClass("active") || (closeQuestion(), theSearch.addClass("active"), 
        TweenLite.to(theBox, .4, {
            height: "auto",
            "margin-top": "16px",
            "margin-bottom": "16px",
            opacity: 1,
            onComplete: function() {
                window.innerWidth < mediumScreen && $("html, body").animate({
                    scrollTop: theSearch.offset().top - 90
                }, 500);
            }
        }), TweenLite.to(theWOHeader, .4, {
            height: "auto"
        }));
    }), woSearchClose.click(function(e) {
        e.stopPropagation(), closeQuestion();
    }), woSearchBar.change(function() {
        var str = woSearchBar.val();
        str = str.replace(/\s+/g, "-").toLowerCase(), woSearchSubmit.prop("href", "/whats-on/search/" + str);
    }), woSearchSubmit.click(function(e) {
        e.preventDefault;
        var str = woSearchBar.val();
        str = str.replace(/\s+/g, "-").toLowerCase(), woSearchSubmit.prop("href", "/whats-on/search/" + str), 
        window.location.href = "/whats-on/search/" + str;
    }), woSearchBar.on("keypress", function(e) {
        if (13 == e.keyCode) {
            var str = woSearchBar.val();
            return str = str.replace(/\s+/g, "-").toLowerCase(), woSearchSubmit.prop("href", "/whats-on/search/" + str), 
            window.location.href = "/whats-on/search/" + str, !1;
        }
    }), woLeft.each(function(i) {
        --i, 0 > i && (i = 2), $(this).click(function(e) {
            e.stopPropagation(), openQuestion(i);
        });
    }), woRight.each(function(i) {
        ++i, i > 2 && (i = 0), $(this).click(function(e) {
            e.stopPropagation(), openQuestion(i);
        });
    }), woQCloseBtns.each(function(i) {
        $(woBtns[i]);
        $(this).click(function(e) {
            e.stopPropagation(), closeQuestion();
        });
    }), selects.each(function(i) {
        var theSelect = $(this), theSelectCopy = $(this).attr("id"), theQuestion = theSelect.parents(".wo-q-item").attr("class").replace("wo-q-item wo-", "").replace(" active", ""), theQuestionArr = selectedChoicesObj[theQuestion];
        ("checked" == theSelect.attr("checked") || theSelect.is(":checked")) && (goButtons.each(function(i) {
            $(this).removeClass("inactive");
        }), resetButtons.each(function(i) {
            $(this).removeClass("inactive");
        }), theSelect.addClass("selected"), theQuestionArr.push(theSelectCopy), updateLink()), 
        theSelect.click(function(e) {
            e.stopPropagation(), theSelect.hasClass("selected") ? (theSelect.removeClass("selected"), 
            $.inArray(theSelectCopy, theQuestionArr) > -1 && theQuestionArr.splice($.inArray(theSelectCopy, theQuestionArr), 1)) : (theSelect.addClass("selected"), 
            theQuestionArr.push(theSelectCopy)), $(".category .selected").length > 0 && goButtons.hasClass("inactive") ? (goButtons.each(function(i) {
                $(this).removeClass("inactive");
            }), resetButtons.each(function(i) {
                $(this).removeClass("inactive");
            })) : $(".category .selected").length < 1 && (goButtons.each(function(i) {
                $(this).addClass("inactive");
            }), resetButtons.each(function(i) {
                $(this).addClass("inactive");
            })), updateLink();
        });
    }), ieCalDropdown.is(":visible")) {
        if (null != ieCalDropdown.val()) {
            var rightNow = moment(), dropStartDate = moment([ moment().year() ]).month(ieCalDropdown.val());
            dropStartDate.isBefore(rightNow) && dropStartDate.year(moment().year() + 1);
            var dropEndDate = moment(dropStartDate).endOf("month");
            selectedChoicesObj.when[0] = moment(dropStartDate).format("YYYY-MM-DD"), selectedChoicesObj.when[1] = moment(dropEndDate).format("YYYY-MM-DD"), 
            updateLink();
        }
        ieCalDropdown.change(function() {
            var rightNow = moment(), dropStartDate = moment([ moment().year() ]).month(ieCalDropdown.val());
            dropStartDate.isBefore(rightNow) && dropStartDate.year(moment().year() + 1);
            var dropEndDate = moment(dropStartDate).endOf("month");
            selectedChoicesObj.when[0] = moment(dropStartDate).format("YYYY-MM-DD"), selectedChoicesObj.when[1] = moment(dropEndDate).format("YYYY-MM-DD"), 
            updateLink();
        });
    }
    resetButtons.each(function(i) {
        $(this).click(function(e) {
            if (!$(this).hasClass("inactive")) {
                $(".category .selected").removeClass("selected").prop("checked", !1), resetButtons.addClass("inactive"), 
                goButtons.addClass("inactive"), woBtns.each(function(i) {
                    $(this).find(".wo-showing").html(woShowingDefault[i]).removeClass("chosen");
                }), startPicker.pikaday.setDate(null), endPicker.pikaday.setDate(null);
                for (var k in selectedChoicesObj) selectedChoicesObj[k] = [];
            }
        });
    }), gridItems.each(function(i) {
        var $this = $(this);
        $this.click({
            theGridItem: $this
        }, clickTheGrid);
    }), allExMods.each(function(i) {
        var $this = $(this);
        $this.off("click", expandInnerDetails).on("click", {
            theMod: $this
        }, expandInnerDetails);
    }), horzScollers.each(function(i) {
        var $this = $(this), hsItem = $this.find(".hs-item").first(), goLeft = $(this).siblings(".hs-transport").find(".hs-left"), goRight = $(this).siblings(".hs-transport").find(".hs-right");
        goLeft.click({
            thePosition: i,
            theFC: hsItem,
            theDirection: "left"
        }, hsScrollMe), goRight.click({
            thePosition: i,
            theFC: hsItem,
            theDirection: "right"
        }, hsScrollMe);
    }), floatNavOn.click(function(e) {
        e.preventDefault(), floatNav.toggleClass("active"), floatNavOn.toggleClass("open"), 
        floatNavOff.toggleClass("closed"), floatSubNav.toggleClass("open"), nextPerf.toggleClass("active");
    }), floatNavOff.click(function(e) {
        e.preventDefault(), floatNav.toggleClass("active"), floatNavOn.toggleClass("open"), 
        floatNavOff.toggleClass("closed"), floatSubNav.toggleClass("open"), nextPerf.toggleClass("active");
    }), playVideo.each(function(i) {
        var $this = $(this);
        $this.click(function(e) {
            $this.fadeOut(150), $(videoCover[i]).fadeOut(150), $(".hero-copy").fadeOut(150);
            var theID = $this.parents(".video-holder").attr("id");
            if (isTouchDevice() === !0) {
                if (callPlayer(theID, function() {}), null !== getFrameID("homeVideoHolder")) {
                    var player;
                    YT_ready(function() {
                        var frameID = getFrameID("homeVideoHolder");
                        frameID && (player = new YT.Player(frameID, {
                            events: {
                                onStateChange: homeVidEnd
                            }
                        }));
                    });
                }
            } else if (callPlayer(theID, function() {
                callPlayer(theID, "playVideo");
            }), callPlayer(theID, "playVideo"), null !== getFrameID("homeVideoHolder")) {
                var player;
                YT_ready(function() {
                    console.log("YT_ready");
                    var frameID = getFrameID("homeVideoHolder");
                    frameID && (player = new YT.Player(frameID, {
                        events: {
                            onStateChange: homeVidEnd
                        }
                    }));
                });
            }
        });
    });
    var unsorted_times = new Array();
    if (plFullList.each(function(i) {
        var thePerfDate = $(this).find(".date").html(), moment_date = moment(thePerfDate), unsorted_time = new Object();
        unsorted_time.time = thePerfDate, unsorted_time.milli = moment_date.valueOf(), unsorted_times.push(unsorted_time), 
        $(this).attr("data-when", moment(thePerfDate, "ddd DD MMM YYYY").format("LL"));
    }), unsorted_times.length > 0) {
        var perfDateSorted = unsorted_times.sort(compareMilli), firstPerfDate = perfDateSorted[0].milli;
        plActiveFilters.when[0] = moment(firstPerfDate).format("YYYY-MM-DD");
        var lastPerfPos = perfDateSorted.length - 1, lastPerfDate = perfDateSorted[lastPerfPos].milli;
        plActiveFilters.when[1] = moment(lastPerfDate).format("YYYY-MM-DD");
    }
    if ($(".pl-calendar").is(":visible") && $("#plStart").length > 0 && void 0 != typeof pikadayResponsive) {
        var sdDefault, edDefault, startDateSet = !1, endDateSet = !1;
        getQueryVariable("plStart") !== !1 && (startDateSet = !0, sdDefault = moment(getQueryVariable("plStart"), "YYYY-MM-DD").toDate()), 
        getQueryVariable("plEnd") !== !1 && (endDateSet = !0, edDefault = moment(getQueryVariable("plEnd"), "YYYY-MM-DD").toDate());
        var startPLDate, endPLDate, updatePLStartDate = function() {
            startPLPicker.pikaday.setStartRange(startPLDate), endPLPicker.pikaday.setStartRange(startPLDate), 
            endPLPicker.pikaday.setMinDate(startPLDate);
        }, updatePLEndDate = function() {
            startPLPicker.pikaday.setEndRange(endPLDate), startPLPicker.pikaday.setMaxDate(endPLDate), 
            endPLPicker.pikaday.setEndRange(endPLDate);
        }, startPLPicker = new pikadayResponsive(document.getElementById("plStart"), {
            format: "D MMM YY",
            outputFormat: "ll",
            classes: "plStartDate",
            pikadayOptions: {
                minDate: new Date(),
                maxDate: new Date(moment(lastPerfDate, "ddd DD MMM YYYY").format("YYYY-MM-DD")),
                defaultDate: sdDefault,
                setDefaultDate: startDateSet,
                onSelect: function() {
                    startPLDate = this.getDate(), updatePLStartDate(), plActiveFilters.when[0] = moment(startPLDate).format("YYYY-MM-DD"), 
                    perfFilter({
                        startDate: plActiveFilters.when[0]
                    });
                }
            }
        }), endPLPicker = new pikadayResponsive(document.getElementById("plEnd"), {
            format: "D MMM YY",
            outputFormat: "ll",
            classes: "plEndDate",
            pikadayOptions: {
                minDate: new Date(),
                maxDate: new Date(moment(lastPerfDate, "ddd DD MMM YYYY").format("YYYY-MM-DD")),
                defaultDate: edDefault,
                setDefaultDate: endDateSet,
                onSelect: function() {
                    endPLDate = this.getDate(), updatePLEndDate(), plActiveFilters.when[1] = moment(endPLDate).format("YYYY-MM-DD"), 
                    perfFilter({
                        endDate: plActiveFilters.when[1]
                    });
                }
            }
        });
        if (startPLPicker.pikaday) var _startPLDate = startPLPicker.pikaday.getDate(), _endPLDate = endPLPicker.pikaday.getDate();
    }
    if (_startPLDate && (startPLDate = _startPLDate, updatePLStartDate()), _endPLDate && (endPLDate = _endPLDate, 
    updatePLEndDate()), plChecks.each(function() {
        var theCheck = $(this);
        theCheck.click(function() {
            perfFilter({
                checkBox: theCheck
            });
        });
    }), perfList.simplebar(), "undefined" != typeof plFilterGroups && plFilterGroups.length > 0) {
        var checkChangeList = [];
        plFilterGroups.each(function(i) {
            var $this = $(this), filterID = this.id, preFilter = getQueryVariable(filterID);
            if (preFilter !== !1) {
                var filterChecks = $this.find(".k-checkbox");
                filterChecks.each(function() {
                    var $newThis = $(this), theCategory = $newThis.attr("name"), theBox = $newThis.prop("id");
                    $newThis.removeAttr("checked"), plActiveFilters[theCategory].splice($.inArray(theBox, plActiveFilters[theCategory]), 1), 
                    theBox == preFilter && checkChangeList.push($newThis);
                });
            }
        });
        for (var i = 0; i < checkChangeList.length; i++) {
            var $this = checkChangeList[i], theCategory = $this.attr("name"), theBox = $this.prop("id");
            getQueryVariable("plStart") !== !1 || getQueryVariable("plEnd") !== !1 ? (plActiveFilters[theCategory].push(theBox), 
            $this.prop("checked", "checked")) : i == checkChangeList.length - 1 ? $this.trigger("click") : (plActiveFilters[theCategory].push(theBox), 
            $this.prop("checked", "checked"));
        }
        getQueryVariable("plStart") !== !1 && getQueryVariable("plEnd") !== !1 ? (plActiveFilters.when[1] = moment(getQueryVariable("plEnd")).format("YYYY-MM-DD"), 
        startPLPicker.setDate(moment(getQueryVariable("plStart"), "YYYY-MM-DD"))) : getQueryVariable("plStart") !== !1 && 0 == getQueryVariable("plEnd") ? startPLPicker.setDate(moment(getQueryVariable("plStart"), "YYYY-MM-DD")) : 0 == getQueryVariable("plStart") && getQueryVariable("plEnd") !== !1 && endPLPicker.setDate(moment(getQueryVariable("plEnd"), "YYYY-MM-DD"));
    }
    $("#socialSharing").sharrre({
        share: {
            facebook: !0,
            twitter: !0,
            pinterest: !0,
            linkedin: !0
        }
    });
}), breakpoint.refreshValue = function() {
    this.value = window.getComputedStyle(document.querySelector("body"), ":before").getPropertyValue("content").replace(/"/g, "");
};

var YT_ready = function() {
    var onReady_funcs = [], api_isReady = !1;
    return function(func, b_before) {
        if (func === !0) for (api_isReady = !0; onReady_funcs.length; ) onReady_funcs.shift()(); else "function" == typeof func && (api_isReady ? func() : onReady_funcs[b_before ? "unshift" : "push"](func));
    };
}();

!function() {
    var s = document.createElement("script");
    s.src = ("https:" == location.protocol ? "https" : "http") + "://www.youtube.com/player_api";
    var before = document.getElementsByTagName("script")[0];
    before.parentNode.insertBefore(s, before);
}(), $.fn.scrollTo = function(target, options, callback) {
    "function" == typeof options && 2 == arguments.length && (callback = options, options = target);
    var settings = $.extend({
        scrollTarget: target,
        offsetTop: 50,
        duration: 500,
        easing: "swing"
    }, options);
    return this.each(function() {
        var scrollPane = $(this), scrollTarget = "number" == typeof settings.scrollTarget ? settings.scrollTarget : $(settings.scrollTarget), scrollY = "number" == typeof scrollTarget ? scrollTarget : scrollTarget.offset().top + scrollPane.scrollTop() - parseInt(settings.offsetTop);
        scrollPane.animate({
            scrollTop: scrollY
        }, parseInt(settings.duration), settings.easing, function() {
            "function" == typeof callback && callback.call(this);
        });
    });
};