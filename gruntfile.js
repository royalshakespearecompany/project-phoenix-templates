module.exports = function(grunt) {
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.initConfig({
        uglify: {
            first_target: {
                options: {
                    beautify: true,
                    mangle: false
                },
                files: {
                    'js/script.js': ['js/comps/*.js']
                } // files
            }, // first target
            second_target: {
                options: {
                    compress: {
                        drop_console: true
                    }
                },
                files: {
                    'js/script.min.js': ['js/comps/*.js']
                } // files
            } // second target
        }, // uglify
        compass: {
            dev: {
                options: {
                    config: 'config.rb'
                } // options
            } // dev
        }, // compass
        watch: {
            options: { 
                livereload: true
            }, // options
            scripts: {
                files: ['js/comps/*.js'],
                tasks: ['uglify']
            }, // script
            sass: {
                files: ['sass/*.scss'],
                tasks: ['compass:dev']
            }, // sass
            html: {
                files: ['*.html']
            }
        } // watch
    }); // initConfig
    
    grunt.registerTask('default', 'watch');
} // exports